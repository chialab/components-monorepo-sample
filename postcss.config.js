module.exports = {
    plugins: [
        require('postcss-css-variables')({ preserve: true }),
        require('postcss-preset-env'),
    ],
};
