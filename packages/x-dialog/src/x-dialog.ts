import { LitElement, html, property, customElement } from 'lit-element';
import 'x-button';
import sheet from './x-dialog.css';

@customElement('x-dialog')
export class XDialog extends LitElement {
    @property()
    label: string;

    adoptStyles() {
        (this.shadowRoot as ShadowRoot & { adoptedStyleSheets: StyleSheet[] }).adoptedStyleSheets = [sheet];
    }

    render() {
        return html`<dialog open>
    <div class="dialog-content"><slot></slot></div>
    <nav class="dialog-nav">
        <x-button label="ok"></x-button>
    </nav>
</dialog>`;
    }
}
