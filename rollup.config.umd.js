import typescript from 'rollup-plugin-typescript2';
import resolve from 'rollup-plugin-node-resolve';
import cssModules from 'rollup-plugin-css-modules';

export default {
    plugins: [
        resolve({
            mainFields: ['module', 'main'],
        }),
        typescript({
            noResolve: true,
            clean: true,
            tsconfigOverride: {
                target: 'es5',
            },
        }),
        cssModules(),
    ],
};
