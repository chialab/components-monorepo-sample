# Sample monorepo for Web Components

In questo repository usiamo NPM, Lerna, TypeScript, Rollup, PostCSS, ESLint e Stylelint per creare un sistema di moduli e componenti interdipendenti. Come libreria per dichiarare i Web Components viene usata LitElement, ma il flusso è del tutto agnostico da questa scelta.

## Tools

* [**NPM**](https://www.npmjs.com/): package manager e script runner (sostituibile con [Yarn](https://yarnpkg.com))
* [**Lerna**](https://github.com/lerna/lerna): monorepo manager
* [**TypeScript**](https://www.typescriptlang.org/): typechecker e transpiler JavaScript (sostituibile con [Babel](https://babeljs.io/))
* [**Rollup**](https://rollupjs.org): module bundler e treeshaker (sostituibile con [Webpack](https://webpack.js.org/))
* [**PostCSS**](https://postcss.org/): transpiler CSS
* [**ESLint**](https://eslint.org/): style checker per JavaScript (sostituibile con [Prettier](https://prettier.io/))
* [**Stylelint**](https://stylelint.io/): style checker per CSS

## Build

Per eseguire la build, usare lo script nel package.json:

```sh
$ npm install
$ npm run build
```

## Publish

Per pubblicare i moduli sul registro NPM (in modalità dry-run) usare lo script:

```sh
$ npm run publish
```

(In questo repository di esempio, invece del comando `lerna publish` viene usato `lerna version` in modo da non caricare *davvero* i pacchetti su NPM).
