import { LitElement, html, property, customElement } from 'lit-element';
import { capitalize } from 'x-utils';
import sheet from './x-button.css';

@customElement('x-button')
export class XButton extends LitElement {
    @property()
    label: string;

    adoptStyles() {
        (this.shadowRoot as ShadowRoot & { adoptedStyleSheets: StyleSheet[] }).adoptedStyleSheets = [sheet];
    }

    render() {
        return html`<button>${capitalize(this.label)}</button>`;
    }
}
