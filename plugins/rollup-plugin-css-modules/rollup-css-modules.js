const { promises } = require('fs');

module.exports = function(options = {}) {
    const transform = typeof options.transform === 'function' ?
        options.transform :
        (async (content) => {
            const postcssrc = require('postcss-load-config');
            const postcss = require('postcss');
            const { plugins, options } = await postcssrc();
            const { css } = await postcss(plugins).process(content, options);
            return css;
        });
    return {
        name: 'css-modules',

        async load(id) {
            if (!id.endsWith('.css')) {

                return;
            }
            const raw = await promises.readFile(id, 'utf-8');
            const css = await transform(raw);
            const encoded = JSON.stringify(css);
            return `import 'construct-style-sheets-polyfill';

const sheet = new CSSStyleSheet();
sheet.replaceSync(${encoded});
export default sheet;`;
        },
    };
};
