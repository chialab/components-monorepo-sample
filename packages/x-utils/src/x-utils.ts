/**
 * Capitalize a string.
 * @param input The string to capitalize.
 */
export function capitalize(input: string): string {
    if (!input) {
        return '';
    }
    return `${input[0].toUpperCase()}${input.substr(1)}`;
}
